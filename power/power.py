#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ToDo: comments to be translated

# Mêmes deux premières lignes que pour hello-world.py
# Force l'utilisation de python3 et de l'encodage UTF-8

# Utilise un module déjà tout fait de python, nommé argparse
# Pour traiter les arguments donnés en ligne de commande (CLI)
import argparse

# voir documentation : https://docs.python.org/fr/3/howto/argparse.html
# la description du programme, pour argparse
desc = 'Takes \'a\' and \'n\' as arguments, returns a to the nth power.'
# initialisation de argparse : description du programme
# et 'écoute' des arguments donnés en ligne de commande (CLI)
# l'objet s'en occupant est nommé 'parser'
parser = argparse.ArgumentParser(description=desc)

# Indique à l'objet 'parser' que l'on attend deux arguments :
# a, nombre de base, et n, puissance
# Tous deux sont de type int (entier) et on donne leur description
# La description sera affichée dans le message d'aide du programme
# voir : nom du programme suivi de l'option '-h' ou '--help'
parser.add_argument('a', type=int, help='base number')
parser.add_argument('n', type=int, help='power')
# Cela donne (ce qui s'affiche en tapant juste 'power.py -h') :
#
# usage: power.py [-h] a n
#
# Takes 'a' and 'n' as arguments, returns a to the nth power.
#
# positional arguments:
#   a           base number
#   n           power
#
# optional arguments:
#   -h, --help  show this help message and exit

# Les arguments reçus en ligne de commande sont passés à la variable 'args'
args = parser.parse_args()


# Définition d'une fonction qui prend deux variables a et n (paramètres)
# et calcule a puissance n (en python : a**n et non a^n)
def power(a, n):
    # note : cette ligne doit être indentée (tabulation à gauche)
    # pour montrer qu'elle appartient à la définition de fonction
    return a**n
# note : ici, argparse traite uniquement les arguments de type int (entiers)
# puisqu'on le lui a précisé. Aussi puisque la fonction power n'est utilisé
# qu'avec les arguments récupérés par argparse, on peut être sûr que seuls
# des entiers (int) seront traités par la fonction (imaginer retourner la
# puissance de deux mots, par exemple, serait un problème...)
# Il est aussi possible avec python3 (depuis 2010) de préciser les types
# attendus dans la definition de la fonction :
#
# def power(a: int, n: int) -> int:
#   return a**n
#
# de cette façon, la fonction indique qu'elle demande des entiers en
# arguments, et calcule un résultat entier, lui aussi


# Dernière ligne du programme : appelle la fonction power()
# en lui fournissant deux arguments : a et n pris parmi les éléments de 'args'
# puis affiche le résultat avec print()
# l'utilisation de la fonction power() est optionnelle puisqu'il serait
# possible d'afficher directement le résultat avec :
#
# print(args.a**args.n)
#
# Toutefois l'utilisation de fonctions est souvent considérée comme une
# meilleure façon de programmer et peut rendre le code plus clair
# (voir : 'programmation orientée objets')
# Cela permettra aussi peut-être de réutiliser la fonction power()
# depuis un autre programme, via 'import'
print(power(args.a, args.n))
