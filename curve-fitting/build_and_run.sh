#!/bin/bash


declare -A available_actions
available_actions[-h]=display_help
available_actions[--help]=display_help
available_actions[--python-help]=call_python_help
available_actions[--clean-up]=prune_images
available_actions[--run-only]=run_container
available_actions[--build-only]=build_container
available_actions[--build-and-run]=build_and_run

# Don't forget to update the help message
# if a function is added or modified


# colors for text formating
default_color="\e[39m"
cyan="\e[36m"
yellow="\e[33m"
green="\e[32m"
red="\e[31m"


# display a help message on how to use this program
function display_help() {
	# "-e" after "echo" allows the use of special characters, like 'tab' (\t)
	echo "Usage: ${0} [<arguments>..]"
	echo ""
	echo "Builds and/or runs the python scripts for automatic linear regression"
  echo "analysis, with stepwise variable selection, in a docker or podman container."
  echo ""
	echo "(This script might also require bash >= v4.0 to work properly,"
  echo "but in most modern operating systems, this shouldn't be a problem)."
	echo ""
	echo "Available build_and_run arguments:"
	echo -e "   -h  --help        --> display this help message"
	echo -e "   --python-help     --> build container and pass -h to the python script"
  echo -e "   --build-only      --> build the container, skip running"
  echo -e "   --run-only        --> skip building, try running only"
  echo -e "   --clean-up        --> prune (clean) the cache of docker / podman"
  echo ""
  echo "Available python script arguments (cf: --python-help):"
  echo -e "   -d  --data-file   --> file to be used for the analysis"
  echo -e "                         ex: ... -d <filename.csv>"
  echo -e "   --nd  --name-delimiter"
  echo -e "                     --> delimiter used to build var names"
  echo -e "                         default: \"___\""
  echo -e "                         (please change it if one of your variables"
  echo -e "                         must have \"___\" in its name)"
  echo -e "                         (note: most special characters won't work...)"
  echo -e "   -i  --with-interactions"
  echo -e "                     --> include interactions between variables"
  echo -e "                         when building the equation of the model"
  echo -e "                         (ex: K + A * x + B * y + C * (x * y) )"
  echo -e "   -s  --with-second-order"
  echo -e "                     --> include second order variables when building"
  echo -e "                         the equation of the model"
  echo -e "                         (ex: K + A * x + B * y + L * (x^2) + M * (y^2) )"
  echo -e "   -f  --forward-first"
  echo -e "                     --> start with all parameters deactivated,"
  echo -e "                         then run forward & backward selections"
  echo -e "                         until the selection is stable."
  echo -e "                         (the default behavior is to start with a"
  echo -e "                         backward selection on all parameters)."
  echo -e "   -o  --once-only"
  echo -e "                     --> only run the (chosen) selection once."
  echo -e "                         (The default behavior without this option"
  echo -e "                         is to run backward & forward selections until"
  echo -e "                         the selection doesn't change anymore)."
  echo ""
  exit 0
}


function docker_podman_check() {
  # first, let's check that either docker or podman can be called
  echo "checking for docker or podman..."
  have_docker="ok"
  have_podman="ok"
  # checking docker
  command -v docker >/dev/null 2>&1 || { have_docker="no"; }
  # checking podman
  command -v podman >/dev/null 2>&1 || { have_podman="no"; }

  # format the results in green or red
  # this is equivalent to if [[ ... ]]; then ...; else ...; fi
  [[ "${have_docker}" == "ok" ]] && have_docker_format="${green}" || have_docker_format="${red}"
  [[ "${have_podman}" == "ok" ]] && have_podman_format="${green}" || have_podman_format="${red}"

  # print the check results formated, with colors
  echo -e "${cyan}check result:   ${have_docker_format}docker: ${have_docker}${cyan}   ${have_podman_format}podman: ${have_podman}${cyan}  ${default_color}"
  if [[ "${have_docker}" != "ok" && "${have_podman}" != "ok" ]]
  then
    # display an error and exit
    echo -e >&2 "${yellow}This script requires either docker or podman.  Aborting.${default_color}"; exit 1;
  fi
}


function build_container() {
  if [[ "${have_podman}" == "ok" ]]
  then
  	echo ""
  	echo -e "${cyan}Building with podman...${default_color}"
  	podman build -t python-curve-fitting .
  elif [[ "${have_docker}" == "ok" ]]
  then
  	echo ""
  	echo -e "${cyan}Building with docker...${default_color}"
  	# note: if docker complains about permissions, use the second line instead
  	docker build -t python-curve-fitting .
  	#sudo docker build -t python-curve-fitting . && sudo docker run --rm --network="host" -it python-curve-fitting
  fi
}


function run_container() {
  if [[ "${have_podman}" == "ok" ]]
  then
  	echo ""
  	echo -e "${cyan}Running with podman...${default_color}"
  	podman run --rm --network="host" -it python-curve-fitting ${@}
  elif [[ "${have_docker}" == "ok" ]]
  then
  	echo ""
  	echo -e "${cyan}Running with docker...${default_color}"
  	# note: if docker complains about permissions, use the second line instead
  	docker run --rm --network="host" -it python-curve-fitting ${@}
  	#sudo docker build -t python-curve-fitting . && sudo docker run --rm --network="host" -it python-curve-fitting
  fi
}


function prune_images() {
  if [[ "${have_podman}" == "ok" ]]
  then
  	echo ""
  	echo -e "${cyan}Cleaning podman image cache...${default_color}"
  	podman image prune --filter label=related=python-curve-fitting --filter "before=python-curve-fitting"
  elif [[ "${have_docker}" == "ok" ]]
  then
  	echo ""
  	echo -e "${cyan}Cleaning docker image cache...${default_color}"
  	# note: if docker complains about permissions, use the second line instead
  	docker image prune --filter label=related=python-curve-fitting --filter "before=python-curve-fitting"
  	#sudo docker build -t python-curve-fitting . && sudo docker run --rm --network="host" -it python-curve-fitting
  fi
}


function call_python_help() {
  build_container && run_container --help
}


function build_and_run() {
  build_container && run_container ${@}
}


function pack_arguments(){
  if [[ "${1}" == "add" ]]
  then
    dash_option=$(eval "expr match \""${@:2}"\" '-'")
    if [[ ${dash_option} -ne 0 ]]
    then
      [[ "${waiting_argument}" != "" ]] && packed_args=("${packed_args[@]}" "${waiting_argument}")
      waiting_argument="${@:2}"
    else
      [[ "${waiting_argument}" != "" ]] && waiting_argument="${waiting_argument} ${@:2}" || waiting_argument="${@:2}"
    fi
  elif [[ "${1}" == "final" ]]
  then
    dash_option=$(eval "expr match \""${@:2}"\" '-'")
    if [[ ${dash_option} -ne 0 ]]
    then
      [[ "${waiting_argument}" != "" ]] && packed_args=("${packed_args[@]}" "${waiting_argument}")
      packed_args=("${packed_args[@]}" "${@:2}")
    else
      [[ "${waiting_argument}" != "" ]] && waiting_argument="${waiting_argument} ${@:2}" || waiting_argument="${@:2}"
      packed_args=("${packed_args[@]}" "${waiting_argument}")
    fi
  fi
}


function sort_packed_arguments() {
  # check if the first argument is in the array of available_actions
  # add the associated function to a list if the argumet is recognized
  # if the argument is not recognized, add it to another list
  # to be passed to the python script
  if [[ ${available_actions[${1}]+_} ]]
  then
    execution_list=("${execution_list[@]}" "${available_actions[${1}]}")
  else
    passed_to_python_script=("${passed_to_python_script[@]}" "${@}")
  fi
}


function process_arguments() {
  # call the help function if -h or --help is anywhere in the arguments
  if echo "${@}" | grep -q -P '(((^|\s)\-h(?=\s|$))|((^|\s)\-\-help(?=\s|$)))'; then display_help; fi

  # process all arguments one by one and "pack them up" into groups
  # for example: [-d] + [data.csv] + [-s] --> [-d data.csv] + [-s]
  args=(${@})
  args_num=${#args[@]}
  packed_args=()
  if [[ ${args_num} -gt 1 ]]
  then
    for arg_index in $(seq 0 $((${args_num} - 2)))
    do
      pack_arguments add ${args[${arg_index}]}
    done
    pack_arguments final ${args[$((${args_num} - 1))]}
  elif [[ ${args_num} -eq 1 ]]
  then
    pack_arguments final ${args[$((${args_num} - 1))]}
  fi

  # now, sort the resulting arguments in two lists :
  # - those that will decide the actions of this shell script
  # - and those that are passed to the python script
  passed_to_python_script=()
  execution_list=()
  for arg_index in $(seq 0 $((${#packed_args[@]} - 1)))
  do
    sort_packed_arguments ${packed_args[${arg_index}]}
  done

  # if the execution list is empty (only python arguments), call build_and_run
  if [[ "${execution_list[@]}" == "" ]]
  then
    execution_list=("build_and_run")
  fi
  # if the execution list contains the call to the python script help,
  # execute this call only (for the user to be able to read the help message)
  if echo "${execution_list[@]}" | grep -q -P '(^|\s)call_python_help(?=\s|$)'
  then
    execution_list=("call_python_help")
  fi

  # finally, run all the commands in the execution list one by one,
  # and pass the arguments every time (each function will decide what to do with those)
  for function_call in ${execution_list[@]}
  do
    eval "${function_call} ${passed_to_python_script[@]}"
  done
}


function main_call() {
  # before all, don't forget to check whether docker (or podman) is installed
  docker_podman_check
  # if the script didn't stop with an error during the check, proceed...
  process_arguments ${@}
}

main_call ${@}
