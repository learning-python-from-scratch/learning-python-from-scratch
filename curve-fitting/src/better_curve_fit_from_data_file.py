#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy
import pandas
from lmfit import Model
import matplotlib as mpl
import matplotlib.pyplot as plt


# needed to display the plot in a web browser
mpl.use('WebAgg')


# very simple and explicit declaration of the equation of the model to be fitted
def model_equation(x, y, a, b, c):
	return a * x + b * y + c


# create a Model from the model_equation function
model = Model(
	model_equation,
	independent_vars=['x','y'],
)


# declare what arguments are parameters and give them initial values.
# Model will know to turn function args `a`, `b`, and `c` into Parameters:
params = model.make_params(
	a=1,
	b=1,
	c=1,
)

## Note:
## you can alter each parameter, for example, fix b or put bounds on a
## for example:
#params['b'].vary = False
#params['b'].value = 5.3
#params['a'].min = -1
#params['a'].max =  1


# values of each parameter during the experiments / in the observed population
# here, we read from a comma-separated file "data.csv", using the pandas library
# /!\ in the data file, each entry was separated with both a comma "," and *spaces* (no tab)
data = pandas.read_csv("data.csv", skipinitialspace=True)

# output values obtained from the experiments or observations
# here, the output (result) of each experiment / observation is kept in the data.csv file
# the result column is named "result" in the file.
output_data = data[data.columns[-1]].values


# run fit
fit_result = model.fit(
	output_data,
	params,
	x=data["x"].values,
	y=data["y"].values,
)

# print and plot results
print("\nfit results:")
print(fit_result.fit_report())
#result.plot(datafmt='--')
#plt.show()

# calculate the R squared coefficient by two different methods
# /!\ the second method needs the variables from "data", be sure to update it after any change
print("\nR^2 :", 1 - fit_result.redchi / numpy.var(data[data.columns[-1]].values, ddof=len(list(fit_result.params.keys()))))
#print("R^2 :", numpy.corrcoef(fit_result.eval(x=data["x"].values, y=data["y"].values), data["result"])[0,1]**2 )

# the values for each parameter can be obtained with:
print(fit_result.values)

# In the prompt that appears when the program finished running, it is possible to
# test the expected result for a given input, with:
#print(fit_result.eval(x=5, y=6))
# (replace the value of x and y by anything you want)

# display a correlation graph
plt.plot(output_data, fit_result.eval(x=data["x"].values, y=data["y"].values), 'bo')
plt.plot(output_data, output_data, color="orange", linestyle="dashed")
plt.title("Correlation graph")
plt.xlabel("actual output (data)")
plt.ylabel("estimated output")
plt.show()
