#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy
import pandas
from lmfit import Model, Parameters
from scipy.stats import f as stats_f
import matplotlib as mpl
import matplotlib.pyplot as plt
import argparse
from termcolor import colored


# needed to display the plot in a web browser
mpl.use('WebAgg')

# Initialize the argument parser with a description of what the program does
program_description = 'Performs a stepwise linear regression analysis on a set of data.'
parser = argparse.ArgumentParser(description=program_description)
# Set possible arguments and their default values
# Note that nargs='?' means 0 or 1 argument of this type can be specified
# const is the value taken when the argument name is given alone (ex: '--load' instead of '--load=0.400' or '--load 0.400')
# default is the value taken when the argument is not specified at all
parser.add_argument(
    '--data-file', '-d',
    type=str,
    nargs='?',
    help='data file on which the analysis is performed. Default: data.csv',
    default='data.csv'
)
parser.add_argument(
    '--name-delimiter', '--nd',
    type=str,
    nargs='?',
    help="delimiter used to build var names when interactions or second order are active. Default: '___'",
    default='___'
)
parser.add_argument(
    '--with-interactions', '-i',
    type=bool,
    nargs='?',
    help='Add interactions between variables (x*y...) to the model. Default: False',
    const=True,
    default=False
)
parser.add_argument(
    '--with-second-order', '-s',
    type=bool,
    nargs='?',
    help='Add second order (x*x, y*y, ...) to the model. Default: False',
    const=True,
    default=False
)
parser.add_argument(
    '--once-only', '-o',
    type=bool,
    nargs='?',
    help='Run the selection of parameters only once with a backward (or forward) selection. Default: False',
    const=True,
    default=False
)
parser.add_argument(
    '--forward-first', '-f',
    type=bool,
    nargs='?',
    help='Run the full selection of parameters, but with a forward selection first. Default: False',
    const=True,
    default=False
)


# Actually parse arguments
args = parser.parse_args()



# values of each parameter during the experiments / in the observed population
# here, we read from a comma-and-tab-separated file "data.ctsv", using the pandas library
# /!\ in the data file, each entry was separated with both a comma "," and a tab "\t" (key just above "Caps Lock" on most keyboards...)
data = pandas.read_csv(args.data_file, skipinitialspace=True)

# output values obtained from the experiments or observations
# here, the output (result) of each experiment / observation is kept in the data.tsv file
# the result column is named "result" in the file.
output_data = data[data.columns[-1]].values



# very simple and explicit declaration of the equation of the model to be fitted
# not used anymore, but kept as an example
##def custom_model_equation(x, y, a, b, c, d, e, f):
##	return a * x + b * y + c * x*y + d * x**2 + e * y**2 + f


# generate a list of independent variables from the data
def auto_generate_independant_var_list(data=data):
	auto_independant_vars = data.columns[:-1].tolist()
	return auto_independant_vars


# generate a list of parameters (coefficients) from the data
# the format is (must be) "coeff_<name of variable(s)>"
def auto_generate_param_name_list(
    independant_var_list=auto_generate_independant_var_list(),
    with_interactions=args.with_interactions,
    with_second_order=args.with_second_order
):
    auto_param_names = ["constant"] + ["coeff_" + var_name for var_name in independant_var_list]
    if with_interactions:
        for index2, var_name2 in enumerate(independant_var_list):
            for index3, var_name3 in enumerate(independant_var_list):
                if (index2 < index3):
                    auto_param_names = auto_param_names + ["coeff_" + var_name2 + args.name_delimiter + var_name3]
    if with_second_order:
        auto_param_names = auto_param_names + ["coeff_" + var_name + args.name_delimiter + var_name for var_name in independant_var_list]
    return auto_param_names


# generate a list of components for the linear equation from the list of parameters
# note: doesn't support independant naming for the list of variables yet
def auto_generate_equation_component_list(
    independant_var_list=auto_generate_independant_var_list(),
    param_name_list=auto_generate_param_name_list(),
):
    if "constant" in param_name_list:
        auto_linear_equation_components = ["constant"]
    else:
        auto_linear_equation_components = []
    for param_name in param_name_list:
        if param_name != "constant":
            nd_var = param_name.replace("coeff_", "").split(args.name_delimiter)
            if len(nd_var) == 2:
                auto_linear_equation_components = auto_linear_equation_components + [param_name + " * " + nd_var[0] + " * " + nd_var[1]]
            elif len(nd_var) == 1:
                auto_linear_equation_components = auto_linear_equation_components + [param_name + " * " + nd_var[0]]
    return auto_linear_equation_components


# run the auto generation functions, and make strings from the results
auto_independant_vars = auto_generate_independant_var_list()
auto_param_names = auto_generate_param_name_list()
auto_linear_equation_components = auto_generate_equation_component_list()
auto_independant_vars_str = str(auto_independant_vars)[1:-1].replace("'", "")
auto_param_names_str = str(auto_param_names)[1:-1].replace("'", "")
auto_linear_equation_str = str(auto_linear_equation_components)[1:-1].replace("'", "").replace(",", " +")


# dynamically define a function with the equation of the linear model, from the strings made earlier
exec(f"""
def auto_linear_model_equation({auto_independant_vars_str + ", " + auto_param_names_str:s}):
	return {auto_linear_equation_str:s}
""")



# create a Model from the model_equation function
model = Model(
	auto_linear_model_equation,
	independent_vars=auto_independant_vars,
)

# create a dictionary that will be used to activate or deactivate parameters
param_dict = {}
for param_name in auto_param_names:
    if args.forward_first:
        # all parameters are inactive at start in the case we run a forward selection first
        param_dict.update({
            param_name: {
                "init_value": 1,
                "activation": False,
                "min": None,
                "max": None,
                "expr": None,
                "brute_step": None
            },
        })
    else:
        # otherwise all parameters are active by default
        param_dict.update({
            param_name: {
                "init_value": 1,
                "activation": True,
                "min": None,
                "max": None,
                "expr": None,
                "brute_step": None
            },
        })


# now, for each parameter listed in the activation dictionary,
# we need to be able to generate a "tupple" (list) of "Parameters"
# that can be understood by the lmfit module
# the format is :
### (name, value, vary, min, max, expr, brute_step)
def generate_parameter_tupple(param_name):
    if param_name in param_dict:
        if param_dict[param_name]["activation"]:
            return (
                param_name,
                param_dict[param_name]["init_value"],
                True,
                param_dict[param_name]["min"],
                param_dict[param_name]["max"],
                param_dict[param_name]["expr"],
                param_dict[param_name]["brute_step"]
            )
        else:
            return (
                param_name,
                0,
                False,
                param_dict[param_name]["min"],
                param_dict[param_name]["max"],
                param_dict[param_name]["expr"],
                param_dict[param_name]["brute_step"]
            )


# assemble all generated individual parameters into a "Parameters" object to be given to lmfit as argument
def generate_test_parameters():
    test_params = Parameters()
    for param_name in auto_param_names:
        test_params.add_many(generate_parameter_tupple(param_name))
    return test_params


def activate_param(param_name):
    if param_name in param_dict:
        param_dict[param_name]["activation"] = True


def deactivate_param(param_name):
    if param_name in param_dict:
        param_dict[param_name]["activation"] = False


def toggle_param(param_name):
    if param_name in param_dict:
        if param_dict[param_name]["activation"] == True:
            param_dict[param_name]["activation"] = False
        else:
            param_dict[param_name]["activation"] = True


# the fit function needs to be given the "input" data as argument.
# in order to avoid manually rewriting the fit function
# every time we change the name or the number of independant variables,
# here we generate a string that contains everything the fit function needs to know
# Bonus: this string can also be used for testing the model later
def auto_generate_function_argument_string(data=data, independant_var_list=auto_independant_vars):
    generated_argument_list = []
    for var_name in independant_var_list:
        if var_name in data:
            generated_argument_list.append(str(var_name + '=data["' + var_name + '"].values'))
    # done with the loop
    return str(generated_argument_list)[1:-1].replace("'", "")


# run fit
exec(f"""
def run_fit():
    return model.fit(
	   output_data,
	   generate_test_parameters(),
	   {auto_generate_function_argument_string():s}
    )
""")


# list of current activation values (can't be used to modify the value in the dictionary):
def get_param_activation_list():
    return list(map(lambda x:x['activation'], param_dict.values()))


# From here, let's implement the stepwise selection algorithm
def backward_selection():
    print(colored("\nBackward selection...", 'cyan'))
    # we start with a certain amount of parameters (probably all)
    # and we will try to deactivate them one by one...
    selected_params = list(numpy.asarray(auto_param_names)[numpy.where(numpy.asarray(get_param_activation_list())==True)])
    if selected_params == []:
        print(colored("No active parameters to run the selection on.", 'yellow'))
        return run_fit(), selected_params
    # this should preserve the constant during the selection process
    ##selected_params.remove("constant")

    continue_selection = True

    def backward_selection_step(continue_selection):
        # run the test once and save the aic value
        aic_0 = run_fit().aic
        # then, deactivate each variable one by one and store the aic each time
        print(f"all current parameters --> aic={aic_0}")
        aic_results = []
        for param_name in selected_params:
            deactivate_param(param_name)
            aic_results.append(run_fit().aic)
            print(f"{param_name:s} off --> aic={aic_results[-1]}")
            activate_param(param_name)
        if numpy.asarray(aic_results).min() < aic_0:
            print(colored(f"deactivating {selected_params[numpy.asarray(aic_results).argmin()]}...\n", 'cyan'))
            deactivate_param(selected_params[numpy.asarray(aic_results).argmin()])
            selected_params.pop(numpy.asarray(aic_results).argmin())
        else:
            continue_selection = False
        return continue_selection

    while continue_selection:
        continue_selection = backward_selection_step(continue_selection)

    print(colored("End of backward selection.", 'cyan'))
    print(colored(f"""Remaining parameters: {str(selected_params)[1:-1].replace("'", "")}""", 'cyan'))
    return run_fit(), selected_params


# From here, let's implement the stepwise selection algorithm
def forward_selection():
    print(colored("\nForward selection...", 'cyan'))
    # we start with a certain amount of parameters (probably all)
    # and we will try to deactivate them one by one...
    unselected_params = list(numpy.asarray(auto_param_names)[numpy.where(numpy.asarray(get_param_activation_list())==False)])
    if unselected_params == []:
        print(colored("No inactive parameters to run the selection on.", 'yellow'))
        return run_fit(), unselected_params
    # this should preserve the constant during the selection process
    ##selected_params.remove("constant")

    continue_selection = True

    def forward_selection_step(continue_selection):
        # run the test once and save the aic value
        aic_0 = run_fit().aic
        # then, deactivate each variable one by one and store the aic each time
        print(f"all current parameters --> aic={aic_0}")
        aic_results = []
        for param_name in unselected_params:
            activate_param(param_name)
            aic_results.append(run_fit().aic)
            print(f"{param_name:s} on --> aic={aic_results[-1]}")
            deactivate_param(param_name)
        if numpy.asarray(aic_results).min() < aic_0:
            print(colored(f"activating {unselected_params[numpy.asarray(aic_results).argmin()]}...\n", 'cyan'))
            activate_param(unselected_params[numpy.asarray(aic_results).argmin()])
            unselected_params.pop(numpy.asarray(aic_results).argmin())
        else:
            continue_selection = False
        return continue_selection

    while continue_selection:
        continue_selection = forward_selection_step(continue_selection)

    print(colored("End of forward selection.", 'cyan'))
    active_params = list(numpy.asarray(auto_param_names)[numpy.where(numpy.asarray(get_param_activation_list())==True)])
    print(colored(f"""Selected parameters: {str(active_params)[1:-1].replace("'", "")}""", 'cyan'))
    return run_fit(), active_params


# actually run the selection loop, and stop when the selected parameters don't change anymore
# or after a maximum of 50 iterations
print(colored("\nRunning composite selection loop...\n", 'cyan'))
# let's make sure the two lists are different at first
param_list_step1 = list(numpy.asarray(auto_param_names)[numpy.where(numpy.asarray(get_param_activation_list())==True)])
param_list_step2 = list(numpy.asarray(auto_param_names)[numpy.where(numpy.asarray(get_param_activation_list())==False)])
if args.forward_first:
    first_selection = forward_selection
    second_selection = backward_selection
else:
    first_selection = backward_selection
    second_selection = forward_selection

if args.once_only:
    fit_result, current_params = first_selection()
else:
    iteration_count = 0
    while param_list_step1 != param_list_step2:
        iteration_count += 1
        fit_result, param_list_step1 = first_selection()
        fit_result, param_list_step2 = second_selection()
        # break the loop if the maximum amount of iterations has been reached
        if iteration_count >= 50:
            break
        # ok
    # done
    current_params = param_list_step2

# print (and plot?) results
print(colored("\nFit results:", 'cyan'))
print(fit_result.fit_report())
# Note: the following can only be used if there is only one independant variable
##fit_result.plot(datafmt='--')
##plt.show()

# calculate the R squared coefficient by two different methods
# /!\ the second method needs the variables from "data", be sure to update it after any change
print("\nR^2 :", 1 - fit_result.redchi / numpy.var(data[data.columns[-1]].values, ddof=len(list(fit_result.params.keys()))))
#print("R^2 :", numpy.corrcoef(fit_result.eval(x=data["x"].values, y=data["y"].values), data["result"])[0,1]**2 )


# generate a string with the actual equation of the model
def get_current_equation_string():
    equation_str = str(auto_generate_equation_component_list(param_name_list=current_params))[1:-1].replace("'", "").replace(",", " +")
    for param_name in current_params:
        equation_str = equation_str.replace(f"{param_name} ", f"{fit_result.result.params[param_name].value:f} ")
    #
    return equation_str


print(colored("\nSimplified equation:", 'cyan'))
print(colored(f"{get_current_equation_string()}\n", 'green'))



#for var_name in list(fit_result.result.params.keys()):
#    if fit_result.result.params[var_name].value != 0:
#	       print(var_name, stats_f.cdf(fit_result.result.params[var_name].value / fit_result.result.params[var_name].stderr, dfn=fit_result.nvarys, dfd=fit_result.nfree, scale=1))

#predicted_results = fit_result.eval(x=data["x"].values, y=data["y"].values)
#SSE = (fit_result.residual**2).sum()
#MSE = SSE / (fit_result.ndata - fit_result.nvarys - 1)
#SSR = ((predicted_results - output_data.mean())**2).sum()
#MSR = SSR / (fit_result.nvarys)
#F = MSR/MSE
#SSTO = SSE + SSR
#MSTO = SSTO / (fit_result.ndata - 1)
#PVE = SSR / SSTO



## display a correlation graph
exec(f"""
def plot_correlation_graph():
    plt.clf()
    plt.plot(output_data, fit_result.eval({auto_generate_function_argument_string()}), 'bo')
    plt.plot([output_data.min(), output_data.max()], [output_data.min(), output_data.max()], color="orange", linestyle="dashed")
    plt.title("Correlation graph")
    plt.xlabel("actual output (data)")
    plt.ylabel("estimated output")
    plt.show()
""")

plot_correlation_graph()

# In the prompt that appears when the program finished running, it is possible to
# test the expected result for a given input, with:
#print(fit_result.eval(x=5, y=6))
# (replace the value of x and y by anything you want)


# Little indication for the user
print(colored("\nRedirecting to the python interpreter...", 'cyan'))
print(colored("Press Ctrl+D or type exit() to leave.", 'cyan'))
