#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# The first line (#!) is used to tell the system
# that it should call python3 to run the program.
# This is probably a common thing for Linux users...
# Maybe it also works on Mac OS,
# but not sure Windows can really use it.
# However, the line will be treated as a comment
# if it is not recognized
# (because it starts with a "#")

# The second line (# -*-) is used to tell python
# that it should use the UTF-8 coding system
# for letters and symbols in the output...
# Without this, accented letters (ex: "à"),
# greek letters (for math, etc.), as well as
# asian charachers (Chinese, Japanese, etc.)
# would not be displayed correctly.

# So, basically, this program just says Hello
print("Hello 世界!")
