#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import _thread
import time
import string

# In this program, we will try to calculate the value of pi,
# by adding an infinite number of small elements
# until the calculation is stopped by the user.

# For that, we will need to use a "thread"
# (a process running separately from the main program)


n = 0

# the function that will calculate the value of pi after n iterations
def calculation(n, result):
    # if n % 2 == 0:
    #    result = result + 4/((2*n)+1)
    # else:
    #    result = result - 4/((2*n)+1)
    return result + ((-1)**n)*4/((2*n)+1)


# function to catch the input on keyboard and share it before we close the thread
def input_thread(a_list):
    input()
    a_list.append(True)


# main function, starting the input thread and the calculation, until the threads stops
def do_calculation_until_keypressed(n):
    a_list = []
    _thread.start_new_thread(input_thread, (a_list,))
    result = 0
    while not a_list:
        result = calculation(n, result)
        print("\r"+str(n)+" "+str(result), end=" ")
        #time.sleep(0.02)
        n += 1


# Real start
do_calculation_until_keypressed(n)
