#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy
from termcolor import colored



# utility function used for checking float numbers from strings
def is_number(s):
    try:
        float(s)
    except ValueError:
        return False
    else:
        return True


# First, let's display a short explanation
print("This program is a (mathematical, multi-factorial) decision helper.")
print("")
print("First, let's identify all the factors that should be taken into account")
print("in the decision-making process...")
print("")

# Then, enter a loop in which the user inputs each factor's name
# save the results in a dictionary
factor_name = ""
factor_counter = 0
factors = {}
factors_in_order = []
print("(Leave the name field empty to end the list of factors)")
while True:
    factor_name = input(f"Name for factor {factor_counter + 1:d}:  ")
    if factor_name == "":
        break
    elif factor_name not in factors:
        factors.update({
            factor_name: {}
        })
        factors_in_order.append(factor_name)
        factor_counter += 1
        while True:
            min_yesno_answer = input(f"\tShould this factor be minimized? (y/n):  ")
            if min_yesno_answer != "y" and min_yesno_answer != "n":
                print(colored("\tthe answer shoud be either \"y\" or \"n\".", "yellow"))
            elif min_yesno_answer == "y":
                factors[factor_name].update({
                    "minimized": True
                })
                break
            elif min_yesno_answer == "n":
                factors[factor_name].update({
                    "minimized": False
                })
                break
            # end of the while loop
    # end of while loop

print("")
# Display a list of all factors
factors_list_string = str(list(factors.keys()))[1:-1]
print(colored(f"Total: {factor_counter:d} factors ({factors_list_string:s})\n", "cyan"))

# stop here if the user didn't add any factor at all
if factor_counter < 1:
    print(colored("At least one factor should be entered in order to go further...", "yellow"))
    exit(0)

# Generate a matrix of the relative importance of each factor
relative_importance = numpy.ones([factor_counter, factor_counter])

# Go through all the possible combinations and set a relative importance for each
if factor_counter >= 2:
    print("Now, let's rate each factor according to their relative importance")
    print("against other given factors...\n")
    for line_i in range(1, factor_counter):
        for column_j in range(0, line_i):
            input_ok: bool = False
            while not input_ok:
                print(f"Would you say that {colored(factors_in_order[line_i], 'green')} is...")
                print(f"{colored('1', 'green')}: really, really less important than {colored(factors_in_order[column_j], 'green')}")
                print(f"{colored('2', 'green')}: really less important than {colored(factors_in_order[column_j], 'green')}")
                print(f"{colored('3', 'green')}: less important than {colored(factors_in_order[column_j], 'green')}")
                print(f"{colored('4', 'green')}: a little bit less important than {colored(factors_in_order[column_j], 'green')}")
                print(f"{colored('5', 'green')}: as important as {colored(factors_in_order[column_j], 'green')}")
                print(f"{colored('6', 'green')}: a little bit more important than {colored(factors_in_order[column_j], 'green')}")
                print(f"{colored('7', 'green')}: more important than {colored(factors_in_order[column_j], 'green')}")
                print(f"{colored('8', 'green')}: really more important than {colored(factors_in_order[column_j], 'green')}")
                print(f"{colored('9', 'green')}: really, really more important than {colored(factors_in_order[column_j], 'green')}")
                relative_importance_input = input("Answer: ")
                print("")
                if relative_importance_input.isdigit() and len(relative_importance_input) == 1 and relative_importance_input != "0":
                    input_ok = True
                    if relative_importance_input == "1":
                        relative_importance[line_i, column_j] = (1/9)
                        relative_importance[column_j, line_i] = 9
                    elif relative_importance_input == "2":
                        relative_importance[line_i, column_j] = (1/7)
                        relative_importance[column_j, line_i] = 7
                    elif relative_importance_input == "3":
                        relative_importance[line_i, column_j] = (1/5)
                        relative_importance[column_j, line_i] = 5
                    elif relative_importance_input == "4":
                        relative_importance[line_i, column_j] = (1/3)
                        relative_importance[column_j, line_i] = 3
                    elif relative_importance_input == "5":
                        relative_importance[line_i, column_j] = 1
                        relative_importance[column_j, line_i] = 1
                    elif relative_importance_input == "6":
                        relative_importance[line_i, column_j] = 3
                        relative_importance[column_j, line_i] = (1/3)
                    elif relative_importance_input == "7":
                        relative_importance[line_i, column_j] = 5
                        relative_importance[column_j, line_i] = (1/5)
                    elif relative_importance_input == "8":
                        relative_importance[line_i, column_j] = 7
                        relative_importance[column_j, line_i] = (1/7)
                    elif relative_importance_input == "9":
                        relative_importance[line_i, column_j] = 9
                        relative_importance[column_j, line_i] = (1/9)
                else:
                    print(colored("The answer must be one digit only, different from zero...\n", "yellow"))
                # end of while loop
            #end of for loop
        # end of for loop
    # end of if condition

# calculate the weights of each factor
# first, sum up the columns of the relative_importance matrix
# divide each entry in the matrix by the sum of its column
# then, take the average of each line
factors_weights = (relative_importance / relative_importance.sum(axis=0)).mean(axis=1)


print(colored(f"Calculated weights:", "cyan"))
for i in range(0, factor_counter):
    print(colored(f"\t{factors_in_order[i]}: {factors_weights[i]}", "cyan"))
    # end of for loop

print("")

# Now, enter a loop where the user should declare the candidates to choose from
print("Now, let's give some details about the possible candidates in this decision...")

# save the results in a dictionary
candidate_name = ""
candidate_counter = 0
candidates = {}
candidates_in_order = []
print("(Leave the name field empty to end the list of candidates)")
while True:
    candidate_name = input(f"Name for candidate {candidate_counter + 1:d}:  ")
    if candidate_name == "":
        break
    elif candidate_name not in candidates:
        # save the candidate in the list and create an entry in the dictionary
        candidates.update({
            candidate_name: {}
        })
        candidates_in_order.append(candidate_name)
        candidate_counter += 1
        # enter the details (value for each factor)
        for f in factors.keys():
            f_value = ""
            while True:
                f_value = input(f"\tvalue for the {f:s} criterion: ")
                if f_value == "":
                    print(colored(f"\tthis value can't be left blank...", "yellow"))
                else:
                    # check if the value is a number (float)
                    if is_number(f_value):
                        # save the value in the dictionary
                        candidates[candidate_name].update({
                            f: float(f_value)
                        })
                        break
                    else:
                        print(colored(f"\tthe input must be a numeric value...", "yellow"))
                # end of while loop
            # end of for loop
    # end of while loop

print("")
# Display a list of all candidates
candidates_list_string = str(list(candidates.keys()))[1:-1]
print(colored(f"Total: {candidate_counter:d} candidates ({candidates_list_string:s})\n", "cyan"))

# stop here if the user didn't add any candidate at all
if candidate_counter < 1:
    print(colored("At least one candidate should be entered in order to perform the analysis...", "yellow"))
    exit(0)

# Create a matrix of the values (columns) for each candidate (lines)
matrix_of_values = numpy.ones([candidate_counter, factor_counter])
for column_j in range(0, factor_counter):
    for line_i in range(0, candidate_counter):
        matrix_of_values[line_i, column_j] = candidates[candidates_in_order[line_i]][factors_in_order[column_j]]
        # end of for loop

    # Add a step to transform the values that should be minimized so that they can turn into values to be maximized
    if factors[factors_in_order[column_j]]["minimized"]:
        sum_in_column = matrix_of_values[:, column_j].sum()
        for line_i in range(0, candidate_counter):
            matrix_of_values[line_i, column_j] = sum_in_column - matrix_of_values[line_i, column_j]
            # end of for loop

    # end of for loop

# normalize the values for each factor (criterion)
normalized_results = ((matrix_of_values / matrix_of_values.sum(axis=0)) * factors_weights / factors_weights.sum()).sum(axis=1)

# check the results:
print("Sorted candidates:")
for i in numpy.flip(normalized_results.argsort()):
    print(f"{colored(candidates_in_order[i], 'green'):s}, probability of being the best choice: {colored(f'{normalized_results[i]*100:f}%', 'green')}")
    # end of for loop
