import plotext as plx
import numpy as np
from statistics import NormalDist
import argparse


description="Create a text plot of a gaussian function for a given mu and sigma"
parser = argparse.ArgumentParser(description=description)
parser.add_argument('--textplot', '-t', nargs='?', type=bool, help="use texn plot (True/*False*)", default=False, const=True)
parser.add_argument('--alternative', '-a', nargs='?', type=str, help="test alternative (*one-sided*/two-sided)", default="one-sided", const="two-sided")
parser.add_argument('mu', metavar='μ', nargs='?', type=np.float32, help="average", default=0)
parser.add_argument('sigma', metavar='σ', nargs='?', type=np.float32, help="standart deviation", default=1)
parser.add_argument('population', metavar='n', nargs='?', type=int, help='total population', default=1)
parser.add_argument('cut_ratio', metavar='p', nargs='?', type=np.float32, help="cut ratio", default=0.75)
parser.add_argument('unit', metavar='u', nargs='?', type=str, help="unit", default="points")
parser.add_argument('population_unit', metavar='v', nargs='?', type=str, help="unit for popuration", default="measurements")
parser.add_argument('analysis_verb', nargs='?', type=str, help="verb (plural form) for statistical insights", default="are rated")
args = parser.parse_args()

if args.textplot==False:
	import matplotlib
	import matplotlib.pyplot as plt
	import seaborn
	matplotlib.use('webagg')

mu = args.mu
sigma = args.sigma
population = args.population
cut_ratio = args.cut_ratio
unit = args.unit
population_unit = args.population_unit
analysis_verb = args.analysis_verb


def gaussian(x, mu, sigma):
	y=1/(sigma * np.sqrt(2 * np.pi)) * np.exp(-((x-mu)**2)/(2*(sigma**2)))
	return y


l=300
x=np.arange(mu-4*sigma, mu+4*sigma, 8*sigma/l)
y=population*gaussian(x, mu, sigma)
#y2=gaussian(x, mu+2, sigma*1.5)

vline_x = (mu, mu, mu, mu, mu, mu, mu, mu, mu, mu)
vline_y = population * np.arange(0, 1/(sigma*np.sqrt(2*np.pi)), 1/(10*sigma*np.sqrt(2*np.pi)))

N = NormalDist(mu, sigma)
point_density = 50
point_x_spacing = 8*sigma/point_density
point_y_spacing = N.pdf(mu)/point_density
if args.alternative=="one-sided":
	cut_value = N.inv_cdf(cut_ratio)
else:
	cut_value = N.inv_cdf(cut_ratio+(1-cut_ratio)/2)
qint = N.quantiles(5)
surface_x = []
surface_y = []
for pxi in np.arange(cut_value, mu+4*sigma, point_x_spacing):
	for pyi in population * np.arange(0, N.pdf(pxi), point_y_spacing):
		surface_x.append(pxi)
		surface_y.append(pyi)
		if args.alternative=="two-sided":
			surface_x.append(-pxi)
			surface_y.append(pyi)


if args.textplot==True:
	#plx.colorless()
	plx.scatter(surface_x, surface_y, color="gray", marker="dot")
	plx.scatter(x, y, color="teal", marker="small")
	#plx.scatter(x, y2, color="teal")
	plx.scatter(vline_x, vline_y, color="tomato", marker="x")
else:
	graph = seaborn.scatterplot(x=surface_x, y=surface_y, color="gray", marker=".")
	seaborn.lineplot(x=x, y=y, color="teal", ax=graph)
	#plx.scatter(x, y2, color="teal")
	plt.vlines(x=mu, ymin=0, ymax=y[np.argmin((x-mu)**2)], color="tomato", linewidth=1.5)
	graph.set(title=f"μ={mu:.2f}, σ={sigma:.2f}, threshold: {cut_ratio*100:.1f}% (α={1-cut_ratio:.3f}, z={cut_value:.3f})")

print("")
print("")

if args.textplot==True:
	plx.show()
else:
	plt.show()

print("")
print(f"μ={mu} ({unit:s})")
print(f"σ={sigma} ({unit:s})")
print(f"20% of the {population_unit} {analysis_verb:s} less than {qint[0]:f} {unit:s}")
print(f"40% of the {population_unit} {analysis_verb:s} less than {qint[1]:f} {unit:s}")
print(f"60% of the {population_unit} {analysis_verb:s} less than {qint[2]:f} {unit:s}")
print(f"80% of the {population_unit} {analysis_verb:s} less than {qint[3]:f} {unit:s}")
print("")
print(f"{cut_ratio:f}% of the {population_unit} {analysis_verb:s} less than {cut_value:f} {unit:s}")
print("")
print("")

