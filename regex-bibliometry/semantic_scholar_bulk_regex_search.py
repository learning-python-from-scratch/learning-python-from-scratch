#!/usr/bin/env python3

import os
import json
import itertools
import argparse
import requests
import re
import pandas
import numpy


### Initialize the argument parser with a description of what the program does
desc = "Script for Semantic Scholar queries with RegEx capabilities"
parser = argparse.ArgumentParser(description=desc)
### Set possible arguments and their default values
### Note that nargs='?' means 0 or 1 argument of this type can be specified
### const is the value taken when the argument name is given alone (ex: '--load' instead of '--load=0.400' or '--load 0.400')
### default is the value taken when the argument is not specified at all
parser.add_argument(
    "--fields",
    "-f",
    type=str,
    help='string defining the fields to be returned. Default: "externalIds,authors,title,year,publicationDate,abstract,publicationTypes,journal,referenceCount,citationCount,isOpenAccess,openAccessPdf"',
    default="externalIds,authors,title,year,publicationDate,abstract,publicationTypes,journal,referenceCount,citationCount,isOpenAccess,openAccessPdf",
    metavar="<F>",
)
parser.add_argument(
    "--query",
    "-q",
    type=str,
    help="query string (regex)",
    default="\\b(?:hip\\W+(?:\\w+\\W+){0,6}?surgery|surgery\\W+(?:\\w+\\W+){0,6}?hip)\\b",
    metavar="<Q>",
)
parser.add_argument(
    "--search-type",
    "-t",
    type=str,
    help='type of transformation for the API query: extended (OR) or limited (AND). Default: "limited"',
    choices=["extended", "limited"],
    default="limited",
    metavar="<T>",
)
parser.add_argument(
    "--max-retrieval",
    "-m",
    type=int,
    help="maximum number of articles to retrieve from the API query. Default: 10000000",
    default=10000000,
    metavar="<M>",
)
parser.add_argument(
    "--regex-search-fields",
    "--rf",
    type=str,
    help='fields in which the regex search is performed. Default: "title,abstract".',
    default="title,abstract",
    metavar="<RF>",
)

### Actually parse arguments
args = parser.parse_args()

### get the (regex) query string from arguments
query = args.query
### get the string describing the desired fields
fields = args.fields
### decide whether to use the AND or OR operator for the extended query
if args.search_type == "limited":
    query_operator = " + "
elif args.search_type == "extended":
    query_operator = " | "
else:
    query_operator = " "  ### equivalent to "+" (AND) in Semantic Scholar's API


### extend the query in order to get all possible results
### The results will later be re-parsed in regex
extended_query = query_operator.join(
    list(
        dict.fromkeys(
            [
                "".join(x).replace(".*", "*").replace(".+", "*").replace(".?", "*")
                for x in re.findall(
                    "(?:\"(.*?)\")|(?:'(.*?)')|(?:([a-zA-Z-]{3,}[.*+?]{0,2}))", query
                )
                ### match whatever is surrounded by quotes, or is made of 3 or more latin alphabet characters
            ]
        )
    )
)

### craft the query URL for Semantic Scholar using the "graph" API (limit 10 million results)
url = f"http://api.semanticscholar.org/graph/v1/paper/search/bulk?query={extended_query}&fields={fields}"

### post the query
print(f'Posting request for extended query: "{extended_query}" ...')
### note: r should be a dictionary with the following keys: 'total', 'token', and 'data'
r = requests.get(url).json()
### note: r seems to be limited to 1000 results at a time, hence the following loop is needed

print(f"Retrieved an estimated {r['total']} documents")
retrieved = 0

### erase the content of papers.jsonl if it exists
if os.path.exists("papers.jsonl"):
    with open("papers.jsonl", "w", encoding="utf-8") as file:
        pass

### loop to write the content of r to a local file (papers.jsonl)
with open(f"papers.jsonl", "a", encoding="utf-8") as file:
    while True:
        if "data" in r:
            retrieved += len(r["data"])
            print(f"Retrieved {retrieved} papers...")
            for paper in r["data"]:
                print(json.dumps(paper), file=file)
        if "token" not in r:
            break
        if retrieved >= args.max_retrieval:
            break
        r = requests.get(f"{url}&token={r['token']}").json()
    # end of while loop


print(f"Done! {retrieved} papers retrieved!")


### Now, read data from the file that was created (note: could have been done fully in memory?)
print("Now parsing data fields from file...")
# with open("papers.jsonl", "r") as file:
#    data = pandas.DataFrame([json.loads(line) for line in file.read().splitlines()])
data = pandas.read_json("papers.jsonl", lines=True)


### turn the authors lists into sub-dataframes
data["authors_df"] = data["authors"].apply(
    lambda x: pandas.DataFrame(x) if len(x) > 0 else numpy.nan
)
data["authors_count"] = data["authors_df"].dropna().apply(lambda x: len(x))


### Perform basic statistics
print("\n---- Data availability statistics ----")
for data_field in data.columns:
    print(
        f"Entries with {data_field}: {len(data)-len(data[data[data_field].isna()])}/{len(data)} ({100*(len(data)-len(data[data[data_field].isna()]))/len(data):.3f}%)"
    )
# end of for loop
print(
    f"Entries with all fields available: {len(data.dropna())}/{len(data)} ({100*len(data.dropna())/len(data):.3f}%)"
)


print("\n---- Authors count statistics ----")
print(
    f"Number of publications with authors data: {len(data[data['authors_count'].notna()])}/{len(data)} ({100*len(data[data['authors_count'].notna()])/len(data):.3f}%)"
)
print(
    f"Single author: {len(data[data['authors_count']==1])}/{len(data[data['authors_count'].notna()])} ({100*len(data[data['authors_count']==1])/len(data[data['authors_count'].notna()]):.3f}%)"
)
print(
    f"Co-authors: {len(data[data['authors_count']>1])}/{len(data[data['authors_count'].notna()])} ({100*len(data[data['authors_count']>1])/len(data[data['authors_count'].notna()]):.3f}%)"
)
print(
    f"Min / Max author count: {data['authors_count'].dropna().min()} / {data['authors_count'].dropna().max()}"
)
print(
    f"Mean (std): {data['authors_count'].dropna().mean():.3f} ({data['authors_count'].dropna().std():.3f})"
)
print(
    f"Median [IQR]: {data['authors_count'].dropna().quantile(q=0.5):.3f} [{data['authors_count'].dropna().quantile(q=0.25):.3f}, {data['authors_count'].dropna().quantile(q=0.75):.3f}]"
)


print("\n---- Publication year statistics ----")
print(
    f"Number of publications with year data: {len(data[data['year'].notna()])}/{len(data)} ({100*len(data[data['year'].notna()])/len(data):.3f}%)"
)
print(f"Min / Max: {data['year'].dropna().min()} / {data['year'].dropna().max()}")
print(
    f"Mean (std): {data['year'].dropna().mean():.3f} ({data['year'].dropna().std():.3f})"
)
print(
    f"Median [IQR]: {data['year'].dropna().quantile(q=0.5):.3f} [{data['year'].dropna().quantile(q=0.25):.3f}, {data['year'].dropna().quantile(q=0.75):.3f}]"
)


### parse the original regex query in the desired fields
for target_field in args.regex_search_fields.split(","):
    data[f"{target_field}_has_query"] = (
        data[target_field].dropna().apply(lambda x: len(re.findall(query, x)) > 0)
    )
# end of for loop

### write the filtered results to files
combination_list = []
for i in range(1, len(args.regex_search_fields.split(",")) + 1):
    combination_list += list(
        map(",".join, itertools.combinations(args.regex_search_fields.split(","), i))
    )
# end of for loop

with open("query.txt", "w") as file:
    file.write(rf"query: {query}")
    file.write("\n\n")
    file.write(rf"extended_query: {extended_query}")

print(f'\nWriting files for papers matching regex query "{query}" ...')
for target_field_str in combination_list:
    print(
        f"Matching query in {target_field_str.replace(',', ' AND ')}: {len(data[(data[[f'{x}_has_query' for x in target_field_str.split(',')]]== True).all(axis=1)])}"
    )
    with open(
        f"papers_matching_{target_field_str.replace(',', '_AND_')}.jsonl", "w"
    ) as file:
        file.write(
            data[
                (
                    data[[f"{x}_has_query" for x in target_field_str.split(",")]]
                    == True
                ).all(axis=1)
            ].to_json(orient="records", lines=True)
        )
    if len(target_field_str.split(",")) > 1:
        print(
            f"Matching query in {target_field_str.replace(',', ' OR ')}: {len(data[(data[[f'{x}_has_query' for x in target_field_str.split(',')]]== True).any(axis=1)])}"
        )
        with open(
            f"papers_matching_{target_field_str.replace(',', '_OR_')}.jsonl", "w"
        ) as file:
            file.write(
                data[
                    (
                        data[[f"{x}_has_query" for x in target_field_str.split(",")]]
                        == True
                    ).any(axis=1)
                ].to_json(orient="records", lines=True)
            )
# end of for loop
