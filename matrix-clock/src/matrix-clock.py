#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#from termcolor import colored
import numpy
from PIL import Image, ImageDraw, ImageFont
from time import sleep
from datetime import datetime
import pytz
import os
import curses

JP = pytz.timezone('Asia/Tokyo')
FR = pytz.timezone('Europe/Paris')

base_char = "0"

#out = widgets.Output(layout={'border': '0px solid black', 'height': '420px', 'width': '1400px'})
stdscr = curses.initscr()
curses.start_color()
curses.use_default_colors()
curses.init_pair(1, curses.COLOR_YELLOW, curses.COLOR_BLACK)
curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_BLACK)

colors_list = {
    "yellow": curses.COLOR_YELLOW,
    "blue": curses.COLOR_BLUE,
    "red": curses.COLOR_RED,
    "green": curses.COLOR_GREEN,
    "magenta": curses.COLOR_MAGENTA,
    "cyan": curses.COLOR_CYAN
}


scr_height, scr_width = stdscr.getmaxyx()
matrix_size = [scr_height -1 , scr_width - 1]


real_matrix = numpy.empty(matrix_size, dtype='|S1')

base_image = Image.new("RGB", (matrix_size[1], matrix_size[0]), "#000000")
real_image = base_image.copy()
bottom_margin = 1
top_margin = 1
left_margin = 0
right_margin = 0
draw = ImageDraw.Draw(real_image)
image_font = ImageFont.truetype("Ubuntu-R.ttf", size=matrix_size[0])

text_start_point_x = matrix_size[1]
text_start_point_y = -1


def fill_matrix(matrix, fill_str):
    if len(fill_str) == 1:
        matrix.fill(fill_str)
    else:
        for char_i in range(0, len(fill_str)):
            for i in range(char_i, matrix.shape[1], len(fill_str)):
                matrix[:, i] = fill_str[char_i]


fill_matrix(real_matrix, base_char)


def draw_text_on_image(text, start_x):
  draw.text([int(start_x), int(text_start_point_y)], text, font=image_font)


# append the matrix to the output line by line
def display_matrix(activated_x, activated_y, base_char, color, stdscr):
  #displaystr = ""
  curses.init_pair(1, colors_list[color], curses.COLOR_BLACK)
  for line in range(0, real_matrix.shape[0]):
    for column in range(0, real_matrix.shape[1]):
      if (line in activated_x) and (column in activated_y[activated_x == line]):
        stdscr.addstr(line, column, real_matrix[line, column], curses.color_pair(1))
      else:
        stdscr.addstr(line, column, real_matrix[line, column], curses.color_pair(2))
    ##
  #print(displaystr, end="")
  #stdscr.addstr("")
  stdscr.refresh()
  #stdscr.getkey()



def animation(text, step, delay, base_char, color, stdscr):
  for pix_step in range(0, matrix_size[1] + int(draw.textlength(text, font=image_font)) + 1, step):
    real_image.paste(base_image, [0, 0])
    draw_text_on_image(text, matrix_size[1] - pix_step)
    # clear the output
    stdscr.clear()
    #os.system('cls||clear')
    #
    # display the master image
    #display(real_image)
    # get pixels as RGB integers
    test_matrix = numpy.array(real_image)[:, :, 0]
    activated_x, activated_y = test_matrix.nonzero()
    display_matrix(activated_x, activated_y, base_char, color, stdscr)
    sleep(delay)


def main_animation(stdscr):
    #print(out)
    base_chars_list = ["Hey! :) ", "You can do it! ", "So don't worry ^^ ", "#"]
    f_colors_list = ["cyan", "magenta", "yellow", "red"]
    for c in range(0, 4):
        base_char = base_chars_list[c]
        fill_matrix(real_matrix, base_char)

        now = datetime.now(FR)  # JP

        current_time = now.strftime("%H:%M")
        #print("Current Time =", current_time)

        animation(current_time, 2, 0.001, base_char, f_colors_list[c], stdscr)


curses.wrapper(main_animation)
