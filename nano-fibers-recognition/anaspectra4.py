#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Starts with argument parsing for faster response time
import argparse

# Description given in help if called without argument
desc = 'Takes a filename as arguments (grayscale picture), and returns the frequency of each scale of gray.'
# Initializing argument parser from argparse
parser = argparse.ArgumentParser(description=desc)
# Setting mendatory argument: filename
parser.add_argument('filename', type=str,
                    help='filename of a grayscale picture')
# Calling parser to get args
args = parser.parse_args()

# Should normally be at the top of the file, but time consuming...
print("Importing libraries...")
from scipy.optimize import curve_fit    # for the gaussian curves separation
from matplotlib.widgets import Slider   # for the slider(s)
import matplotlib.gridspec as gridspec  # for the layout
import imageio.v3 as imageio            # for reading the picture
import numpy as np                      # for the calculations, etc.
import matplotlib.pyplot as plt         # for the graphical outpout
import matplotlib                       # for the graphics and interface
matplotlib.use('webagg')    # #WebAgg #pdf? #TkAgg #defines the output interface

print("Reading picture file...")
spc = imageio.imread(args.filename)

print("Generating graph...")
# Setting variables
a_min = 0      # the minimial value of the paramater a (for the slider)
a_max = 49     # the maximal value of the paramater a
a_init = 10    # the value of the parameter a to be used initially, when the graph is created
b_min = 0      # the minimial value of the paramater a (for the slider)
b_max = 1      # the maximal value of the paramater a
b_init = 0.5   # the value of the parameter a to be used initially, when the graph is created


# Theoretical function that is the sum of two gaussians
# (colors of fibers + colors of the background = colors of the whole picture)
def func(x, a1, b1, c1, a2, b2, c2):
    return a1*np.exp(-((x-b1)**2)/(2*((c1)**2)))+a2*np.exp(-((x-b2)**2)/(2*((c2)**2)))


# Definition of a gaussian function
def gaussian(x, a1, b1, c1):
    return a1*np.exp(-((x-b1)**2)/(2*((c1)**2)))


# Sensibility and limits of the 2nd slider (detection sensibility)
# Trying to do proper (de-)normalization, and handle the case
# where the returned number is negative
def f_slider_b(mu, sigma, b):
    if ((((b*2)-1)*3*sigma)+mu) > 0:
        return ((((b*2)-1)*3*sigma)+mu)
    else:
        return 0


# Setting up the plots parameters
fig = plt.figure(figsize=(15, 6))
# Creating a grid with specific ratios for the plots
gs = gridspec.GridSpec(4, 4, width_ratios=[
                       1, 3, 3, 1], height_ratios=[16, 1, 1, 1])
# Setting up the window's margins
gs.update(left=0.00, bottom=0.06, right=0.95,
          top=0.95, wspace=0.05, hspace=0.3)
# Declaring "subplots" (sections of the main window) and their geometry
ax1 = plt.subplot(gs[0, :2])
ax2 = plt.subplot(gs[0, 2:])
ax3 = plt.subplot(gs[2, 1:])
ax4 = plt.subplot(gs[3, 1:])
tx1 = plt.subplot(gs[2, 0])
tx2 = plt.subplot(gs[3, 0])
# Removing axes from some subplots (picture and texts)
ax1.axis('off')
tx1.axis('off')
tx2.axis('off')
# Declaring sliders from matplotlib.widgets
a_slider = Slider(ax3,      # the axes object containing the slider
                  '',            # the name of the slider parameter
                  a_min,          # minimal value of the parameter
                  a_max,          # maximal value of the parameter
                  valinit=a_init,  # initial value of the parameter
                  #color="blue"
                  )
b_slider = Slider(ax4,      # the axes object containing the slider
                  '',            # the name of the slider parameter
                  b_min,          # minimal value of the parameter
                  b_max,          # maximal value of the parameter
                  valinit=b_init,  # initial value of the parameter
                  #color="orange"
                  )
# PLacing text before the sliders
plt.text(1.0, 0.0, 'border (%)', ha='right',
         fontsize=20, transform=tx1.transAxes)
plt.text(1.0, 0.0, 'sensibility', ha='right',
         fontsize=20, transform=tx2.transAxes)

# Given that the picture is a square, spc.shape[0] gives its dimentions
# The dimention is scaled using the percentage from slider a (1st slider)
d = int(spc.shape[0]*a_slider.val//100)
# the original picture (spc) is kept, and the scaled picture is named spd
spd = spc[d:-d, d:-d, :]
# in order to avoid modifying spd, a copy is made in spdf
spdf = spd.copy()
# counting the (unique) color values of spd, and their frequency (occurences)
color, counts = np.unique(spd[:, :, 0], return_counts=True)
# Putting the color information in a single table: [[color value, count]]
cfreq = np.asarray((color, counts)).T
# in order to make graphs and calculations, separate xdata and ydata
# xdata = color values = list(color) = list(cfreq[:, 0])
# ydata = color count = list(counts) = list(cfreq[:, 1])
xdata = list(cfreq[:, 0])
ydata = list(cfreq[:, 1])
# fitting "func" (sum of two gaussians) on the calculated color count
# the function curve_fit is taken from scipy.optimize
# popt = optimal values for func parameters: a1, b1, c1, a2, b2, c2
# /!\ the two gaussians are not always in the same order
# pcov = estimated covariance of popt
popt, pcov = curve_fit(func, xdata, ydata, bounds=(
        0, [spd.shape[0]*spd.shape[1], 255, 255, spd.shape[0]*spd.shape[1], 255, 255]
    ))
# perr = estimated standard deviation error on popt
# #perr = np.sqrt(np.diag(pcov))   # not used for now, but might be useful
# Declaring two gaussian "objects" from the results of the curve fitting
# The order hasn't been checked yet
y1 = gaussian(xdata, *popt[:3])
y2 = gaussian(xdata, *popt[3:])
# calculating the mediane, but not used so far
#med = int(np.mean(list(cfreq[cfreq[:,1]>cfreq[:,1].max()*0.95,0])))

# Select the second Subplot (where the histogram is to be displayed)
plt.sca(ax2)
# Displays an histogram of the data, replaced with a plot
#plt.bar(cfreq[:,0],cfreq[:,1])
# Displays a plot of xdata and xdata
plt.plot(xdata, ydata, color="grey")
# Plots the fitted curve (func)
plt.plot(xdata, func(xdata, *popt), color="orange")
# checks which of the two gaussians has the smallest mediane
# it is supposed here that the fibers are darker than the background
# popt[1] = a1 or a2, popt[4] = a2 or a1
if popt[1] < popt[4]:
    # plot the two gaussians with different colors
    plt.plot(xdata, y1, color="red")
    plt.plot(xdata, y2, color="blue")
    # Plots a vertical line in grey
    # where the difference between the two gaussians is the greatest
    plt.axvline(x=xdata[np.argmax(abs(
        y1[np.asarray(xdata) < popt[1]]-y2[np.asarray(xdata) < popt[1]]))], color="grey")
    # Plots another vertical line in red
    # at the mediane of the gaussian of fibers' colors
    plt.axvline(x=f_slider_b(popt[1], popt[2], b_slider.val), color="red")
    # Colors in Red the points of the scaled picture
    # that are darker than the value selected on slider b (2nd slider)
    # the value has been centered and scaled around the fibers' gaussian curve
    spdf[spd[:, :, 1] < f_slider_b(popt[1], popt[2], b_slider.val)] = [255, 0, 0]
else:
    # Same as previously, but the fibers' gaussian curve is the 2nd one here
    # and the corresponding mediane value (a1) is given by popt[4]
    # (could make a function for that?)
    plt.plot(xdata, y1, color="blue")
    plt.plot(xdata, y2, color="red")
    plt.axvline(x=xdata[np.argmax(abs(
        y1[np.asarray(xdata) < popt[4]]-y2[np.asarray(xdata) < popt[4]]))], color="grey")
    plt.axvline(x=f_slider_b(popt[4], popt[5], b_slider.val), color="red")
    spdf[spd[:, :, 1] < f_slider_b(popt[4], popt[5], b_slider.val)] = [255, 0, 0]

# Placing a title under the second Subplot (ax2), centered
plt.text(0.5, -0.15, 'Spectral analysis', ha='center',
         fontsize=20, transform=ax2.transAxes)

# Selecting the first Subplot (ax1): image display
plt.sca(ax1)
# Display the scaled and modified image (colored selected pixels)
imageobj = plt.imshow(spdf)
# Displaying the picture's filename as a title under the picture, centered
plt.text(0.5, -0.15, args.filename, ha='center',
         fontsize=20, transform=ax1.transAxes)


# Definition of an update function to be called on slider change
# not very clear what "a" is refering to here, maybe the slider widget id
def update(a):
    # the value of the slider a is passed by the widget as a_slider.val
    # here, if the value of the slider is lower than or equal to 0.1,
    # take the original picture size
    # otherwise, scale the picture accordingly
    if a_slider.val > 0.1:
        d = int(spc.shape[0]*a_slider.val//100)
        spd = spc[d:-d, d:-d, :]
        spdf = spd.copy()
    else:
        spd = spc
        spdf = spd.copy()
    # Now about updating color frequencies from the scaled picture
    # Same calculation as earlier (could make a function for that?)
    color, counts = np.unique(spd[:, :, 0], return_counts=True)
    cfreq = np.asarray((color, counts)).T
    xdata = list(cfreq[:, 0])
    ydata = list(cfreq[:, 1])
    # Also update the curve fitting and the two gaussians
    popt, pcov = curve_fit(func, xdata, ydata, bounds=(
        0, [spd.shape[0]*spd.shape[1], 255, 255, spd.shape[0]*spd.shape[1], 255, 255]))
    y1 = gaussian(xdata, *popt[:3])
    y2 = gaussian(xdata, *popt[3:])
    #med = int(np.mean(list(cfreq[cfreq[:,1]>cfreq[:,1].max()*0.95,0])))

    # Selects the second Subplot (graph / plot)
    plt.sca(ax2)
    # Clears the currently displayed plot not to draw on top of it
    ax2.clear()
    #plt.bar(cfreq[:,0],cfreq[:,1])
    # Plotting the updating functions
    plt.plot(xdata, ydata, color="grey")
    plt.plot(xdata, func(xdata, *popt), color="orange")
    # again, this code for the selection of the fibers' gaussian curve
    # is identical to the one ran at initialization
    # could make a single function for that to reduce complexity
    if popt[1] < popt[4]:
        plt.plot(xdata, y1, color="red")
        plt.plot(xdata, y2, color="blue")
        plt.axvline(x=xdata[np.argmax(abs(
            y1[np.asarray(xdata) < popt[1]]-y2[np.asarray(xdata) < popt[1]]))], color="grey")
        plt.axvline(x=f_slider_b(popt[1], popt[2], b_slider.val), color="red")
        spdf[spd[:, :, 1] < f_slider_b(popt[1], popt[2], b_slider.val)] = [255, 0, 0]
    else:
        plt.plot(xdata, y1, color="blue")
        plt.plot(xdata, y2, color="red")
        plt.axvline(x=xdata[np.argmax(abs(
            y1[np.asarray(xdata) < popt[4]]-y2[np.asarray(xdata) < popt[4]]))], color="grey")
        plt.axvline(x=f_slider_b(popt[4], popt[5], b_slider.val), color="red")
        spdf[spd[:, :, 1] < f_slider_b(popt[4], popt[5], b_slider.val)] = [255, 0, 0]
    # Since the plot has been cleared, printing the title again is necessary
    plt.text(0.5, -0.15, 'Spectral analysis', ha='center',
             fontsize=20, transform=ax2.transAxes)
    # Selects the first subplot (picture display)
    plt.sca(ax1)
    # Update the currently displayed picture
    imageobj.set_data(spdf)


# On Slider_a update, call the update function
a_slider.on_changed(update)

# On Slider_b update, call the update function
b_slider.on_changed(update)

# End of the initialization, show the "plot" / window
plt.show()
