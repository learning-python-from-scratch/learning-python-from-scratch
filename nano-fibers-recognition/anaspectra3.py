#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ToDo: Comments to be added

import argparse

desc = 'Takes a filename as arguments (grayscale picture), and returns the frequency of each scale of gray.'
parser = argparse.ArgumentParser(description=desc)
parser.add_argument('filename', type=str, help='filename of a grayscale picture')
args = parser.parse_args()

print("Importing libraries...")
import matplotlib #for the graphics and interface
matplotlib.use('webagg') #WebAgg #pdf? #TkAgg #defines the output interface
import matplotlib.pyplot as plt #for the graphical outpout
import numpy as np #for the calculations, etc.
import imageio.v3 as imageio #for reading the picture
import matplotlib.gridspec as gridspec #for the layout
from matplotlib.widgets import Slider #for the slider(s)
from scipy.optimize import curve_fit #for the gaussian curves separation

print("Reading picture file...")
spc = imageio.imread(args.filename)

print("Generating graph...")
a_min = 0    # the minimial value of the paramater a (for the slider)
a_max = 49   # the maximal value of the paramater a
a_init = 10   # the value of the parameter a to be used initially, when the graph is created
b_min = 0    # the minimial value of the paramater a (for the slider)
b_max = 1   # the maximal value of the paramater a
b_init = 0.5   # the value of the parameter a to be used initially, when the graph is created
def func(x, a1, b1, c1, a2, b2, c2):
    return a1*np.exp(-((x-b1)**2)/(2*((c1)**2)))+a2*np.exp(-((x-b2)**2)/(2*((c2)**2)))

def gaussian(x, a1, b1, c1):
    return a1*np.exp(-((x-b1)**2)/(2*((c1)**2)))

def f_slider_b(x,b):
    return 2*b*x

fig = plt.figure(figsize=(15, 6))
gs = gridspec.GridSpec(4, 4, width_ratios=[1, 3, 3, 1], height_ratios=[16, 1, 1, 1])
gs.update(left=0.00, bottom=0.06, right=0.95, top=0.95, wspace=0.05, hspace=0.3)
ax1 = plt.subplot(gs[0, :2])
ax2 = plt.subplot(gs[0, 2:])
ax3 = plt.subplot(gs[2, 1:])
ax4 = plt.subplot(gs[3, 1:])
tx1 = plt.subplot(gs[2, 0])
tx2 = plt.subplot(gs[3, 0])
ax1.axis('off')
tx1.axis('off')
tx2.axis('off')
a_slider = Slider(ax3,      # the axes object containing the slider
                  '',            # the name of the slider parameter
                  a_min,          # minimal value of the parameter
                  a_max,          # maximal value of the parameter
                  valinit=a_init,  # initial value of the parameter
                  #color="blue"
                 )
b_slider = Slider(ax4,      # the axes object containing the slider
                  '',            # the name of the slider parameter
                  b_min,          # minimal value of the parameter
                  b_max,          # maximal value of the parameter
                  valinit=b_init,  # initial value of the parameter
                  #color="orange"
                 )
plt.text(1.0, 0.0, 'border (%)', ha='right', fontsize=20, transform=tx1.transAxes)
plt.text(1.0, 0.0, 'sensibility', ha='right', fontsize=20, transform=tx2.transAxes)
d=1024*a_slider.val//100
spd = spc[d:-d,d:-d,:]
spdf = spd.copy()
color, counts = np.unique(spd[:,:,0], return_counts=True)
cfreq = np.asarray((color, counts)).T
xdata=list(cfreq[:,0])
ydata=list(cfreq[:,1])
popt, pcov = curve_fit(func, xdata, ydata, bounds=(0,[spd.shape[0]*spd.shape[1],255,255,spd.shape[0]*spd.shape[1],255,255]))
y1=gaussian(xdata, *popt[:3])
y2=gaussian(xdata, *popt[3:])
#med = int(np.mean(list(cfreq[cfreq[:,1]>cfreq[:,1].max()*0.95,0])))

plt.sca(ax2)
#plt.bar(cfreq[:,0],cfreq[:,1])
plt.plot(xdata, ydata, color="grey")
plt.plot(xdata, func(xdata, *popt), color="orange")
if popt[1] < popt[4]:
    plt.plot(xdata, y1, color="red")
    plt.plot(xdata, y2, color="blue")
    plt.axvline(x=xdata[np.argmax(abs(y1[np.asarray(xdata)<popt[1]]-y2[np.asarray(xdata)<popt[1]]))], color="grey")
    plt.axvline(x=f_slider_b(b_slider.val,popt[1]), color="red")
    spdf[spd[:,:,1]<f_slider_b(b_slider.val,popt[1])]=[255,0,0]
else:
    plt.plot(xdata, y1, color="blue")
    plt.plot(xdata, y2, color="red")
    plt.axvline(x=xdata[np.argmax(abs(y1[np.asarray(xdata)<popt[4]]-y2[np.asarray(xdata)<popt[4]]))], color="grey")
    plt.axvline(x=f_slider_b(b_slider.val,popt[4]), color="red")
    spdf[spd[:,:,1]<f_slider_b(b_slider.val,popt[4])]=[255,0,0]
plt.text(0.5, -0.15, 'Spectral analysis', ha='center', fontsize=20, transform=ax2.transAxes)
plt.sca(ax1)
imageobj = plt.imshow(spdf)
plt.text(0.5, -0.15, args.filename, ha='center', fontsize=20, transform=ax1.transAxes)

def update(a):
    if a_slider.val > 0.1:
        d=int(1024*a_slider.val//100)
        spd = spc[d:-d,d:-d,:]
        spdf = spd.copy()
    else:
        spd = spc
        spdf = spd.copy()
    # Now about color frequencies
    color, counts = np.unique(spd[:,:,0], return_counts=True)
    cfreq=np.asarray((color, counts)).T
    xdata=list(cfreq[:,0])
    ydata=list(cfreq[:,1])
    popt, pcov = curve_fit(func, xdata, ydata, bounds=(0,[spd.shape[0]*spd.shape[1],255,255,spd.shape[0]*spd.shape[1],255,255]))
    y1=gaussian(xdata, *popt[:3])
    y2=gaussian(xdata, *popt[3:])
    #med = int(np.mean(list(cfreq[cfreq[:,1]>cfreq[:,1].max()*0.95,0])))

    plt.sca(ax2)
    ax2.clear()
    #plt.bar(cfreq[:,0],cfreq[:,1])
    plt.plot(xdata, ydata, color="grey")
    plt.plot(xdata, func(xdata, *popt), color="orange")
    if popt[1] < popt[4]:
        plt.plot(xdata, y1, color="red")
        plt.plot(xdata, y2, color="blue")
        plt.axvline(x=xdata[np.argmax(abs(y1[np.asarray(xdata)<popt[1]]-y2[np.asarray(xdata)<popt[1]]))], color="grey")
        plt.axvline(x=f_slider_b(b_slider.val,popt[1]), color="red")
        spdf[spd[:,:,1]<f_slider_b(b_slider.val,popt[1])]=[255,0,0]
    else:
        plt.plot(xdata, y1, color="blue")
        plt.plot(xdata, y2, color="red")
        plt.axvline(x=xdata[np.argmax(abs(y1[np.asarray(xdata)<popt[4]]-y2[np.asarray(xdata)<popt[4]]))], color="grey")
        plt.axvline(x=f_slider_b(b_slider.val,popt[4]), color="red")
        spdf[spd[:,:,1]<f_slider_b(b_slider.val,popt[4])]=[255,0,0]
    plt.text(0.5, -0.15, 'Spectral analysis', ha='center', fontsize=20, transform=ax2.transAxes)
    plt.sca(ax1)
    imageobj.set_data(spdf)

a_slider.on_changed(update)

b_slider.on_changed(update)

plt.show()
