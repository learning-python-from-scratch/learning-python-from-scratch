#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ToDo: Comments to be added

import argparse

desc = 'Takes a filename as arguments (grayscale picture), and returns the frequency of each scale of gray.'
parser = argparse.ArgumentParser(description=desc)
parser.add_argument('filename', type=str, help='filename of a grayscale picture')
args = parser.parse_args()

print("Importing libraries...")
import matplotlib
matplotlib.use('webagg') #WebAgg #pdf? #TkAgg
import matplotlib.pyplot as plt
import numpy as np
import imageio.v3 as imageio
import matplotlib.gridspec as gridspec
from matplotlib.widgets import Slider

print("Reading picture file...")
spc = imageio.imread(args.filename)

print("Generating graph...")
a_min = 0    # the minimial value of the paramater a (for the slider)
a_max = 49   # the maximal value of the paramater a
a_init = 10   # the value of the parameter a to be used initially, when the graph is created
fig = plt.figure(figsize=(15, 6))
gs = gridspec.GridSpec(2, 2, height_ratios=[16, 1])
gs.update(left=0.05, bottom=0.06, right=0.95, top=0.95, wspace=0.05, hspace=0.3)
ax1 = plt.subplot(gs[0, 0])
ax2 = plt.subplot(gs[0, 1])
ax3 = plt.subplot(gs[1, :])
ax1.axis('off')
a_slider = Slider(ax3,      # the axes object containing the slider
                  '',            # the name of the slider parameter
                  a_min,          # minimal value of the parameter
                  a_max,          # maximal value of the parameter
                  valinit=a_init,  # initial value of the parameter
                 )
plt.text(0.5, -1.0, 'border (%)', ha='center', fontsize=20, transform=ax3.transAxes)
d=1024*a_slider.val//100
spd = spc[d:-d,d:-d,:]
plt.sca(ax1)
imageobj = plt.imshow(spd)
plt.text(0.5, -0.12, args.filename, ha='center', fontsize=20, transform=ax1.transAxes)
#print("Calculating color frequencies...")
color, counts = np.unique(spd[:,:,0], return_counts=True)
cfreq = np.asarray((color, counts)).T
plt.sca(ax2)
plt.bar(cfreq[:,0],cfreq[:,1])
plt.text(0.5, -0.12, 'Spectral analysis', ha='center', fontsize=20, transform=ax2.transAxes)

def update(a):
    if a_slider.val > 0.1:
        d=int(1024*a_slider.val//100)
        spd = spc[d:-d,d:-d,:]
    else:
        spd = spc
    plt.sca(ax1)
    imageobj.set_data(spd)
    # Now about color frequencies
    color, counts = np.unique(spd[:,:,0], return_counts=True)
    cfreq=np.asarray((color, counts)).T
    plt.sca(ax2)
    ax2.clear()
    plt.bar(cfreq[:,0],cfreq[:,1])


a_slider.on_changed(update)

plt.show()
