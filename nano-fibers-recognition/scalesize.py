#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Starts with argument parsing for faster response time
import argparse

# Description given in help if called without argument
desc = 'Takes a filename as arguments (grayscale picture), and returns the frequency of each scale of gray.'
# Initializing argument parser from argparse
parser = argparse.ArgumentParser(description=desc)
# Setting mendatory argument: filename
parser.add_argument('filename', type=str,
                    help='filename of a grayscale picture')
# Calling parser to get args
args = parser.parse_args()

# Should normally be at the top of the file, but time consuming...
print("Importing libraries...")
import imageio.v3 as imageio                          # for reading the picture
import numpy as np                      # for the calculations, etc.
import sys

print("Reading picture file...")
spc = imageio.imread(args.filename)

dLowLimit = 240
topCount = 5

print("Searching scale position...")
count = 0
increment = 0
brl = spc.shape[0:2]
val = np.asarray([0, 0, 0])
while count < topCount:
    brl = [brl[0]-1, brl[1]-1]
    if brl[0] < 0:
        if increment < spc.shape[1]:
            increment += 32
            brl[0] = spc.shape[0] - 1
            brl[1] = spc.shape[1] - 1 - increment
        else:
            # print("Scale could not be found")
            sys.exit("Scale not found")
    if brl[1] < 0:
        brl[1] = spc.shape[1] - 1
    val = spc[brl[0], brl[1], :]
    if not (val <= np.asarray([dLowLimit, dLowLimit, dLowLimit])).any():
        count += 1
    else:
        count = 0

# Note: it seems x and y coordibnates are "inverted" (not intuitive)
print("Seeking left border...")
while not (val <= np.asarray([dLowLimit, dLowLimit, dLowLimit])).any():
    brl = [brl[0], brl[1]-1]
    val = spc[brl[0], brl[1], :]

# Saving the current position
# And returning to the "inside" of the scale
brl = [brl[0], brl[1]+1]
val = spc[brl[0], brl[1], :]
left = brl[1]

print("Seeking right border...")
while not (val <= np.asarray([dLowLimit, dLowLimit, dLowLimit])).any():
    brl = [brl[0], brl[1]+1]
    val = spc[brl[0], brl[1], :]

# Saving the current position
# And returning to the "inside" of the scale
brl = [brl[0], brl[1]-1]
val = spc[brl[0], brl[1], :]
right = brl[1]

print("Size of the current scale: ", right-left+1, "pixels  (position:", left, "-", right, ",", brl[0], ")")
