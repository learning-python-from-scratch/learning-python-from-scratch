#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ToDo: comments to be added

# Starts with argument parsing for faster response time
import argparse

# Description given in help if called without argument
desc = 'Takes a filename as arguments (grayscale picture), and returns the frequency of each scale of gray.'
# Initializing argument parser from argparse
parser = argparse.ArgumentParser(description=desc)
# Setting mendatory argument: filename
parser.add_argument('filename', type=str, help='filename of a grayscale picture')
# Calling parser to get args
args = parser.parse_args()

# Should normally be at the top of the file, but time consuming...
print("Importing libraries...")
import matplotlib #for the graphics and interface
matplotlib.use('webagg') #WebAgg #pdf? #TkAgg #defines the output interface
import matplotlib.pyplot as plt #for the graphical outpout
import numpy as np #for the calculations, etc.
import imageio.v3 as imageio #for reading the picture
import matplotlib.gridspec as gridspec #for the layout
from scipy.optimize import curve_fit #for the gaussian curves separation
from matplotlib.widgets import Slider #for the slider(s)

print("Reading picture file...")
spc = imageio.imread(args.filename)

print("Generating graph...")
# Setting variables
a_min = 0    # the minimial value of the paramater a (for the slider)
a_max = (spc.shape[0]-2*spc.shape[0]*10//100)-100   # the maximal value of the paramater a
a_init = 100   # the value of the parameter a to be used initially, when the graph is created
b_min = 3    # the minimial value of the paramater a (for the slider)
b_max = (spc.shape[0]-2*spc.shape[0]*10//100)-4   # the maximal value of the paramater a
b_init = 420   # the value of the parameter a to be used initially, when the graph is created

# Setting up the plots parameters
fig = plt.figure(figsize=(15, 6))
# Creating a grid with specific ratios for the plots
gs = gridspec.GridSpec(4, 1, width_ratios=[1], height_ratios=[16, 2, 1, 1])
# Setting up the window's margins
gs.update(left=0.05, bottom=0.06, right=0.95, top=0.90, wspace=0.00, hspace=0.2)
# Declaring "subplots" (sections of the main window) and their geometry
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])
ax3 = plt.subplot(gs[2])
ax4 = plt.subplot(gs[3])
# Removing axes from some subplots (picture and texts)
ax2.axis('off')


# Theoretical function that is the sum of two gaussians
# (colors of fibers + colors of the background = colors of the whole picture)
def func(x, a1, b1, c1, a2, b2, c2):
    return a1*np.exp(-((x-b1)**2)/(2*((c1)**2)))+a2*np.exp(-((x-b2)**2)/(2*((c2)**2)))


# Definition of a gaussian function
def gaussian(x, a1, b1, c1):
    return a1*np.exp(-((x-b1)**2)/(2*((c1)**2)))


# Declaring sliders from matplotlib.widgets
a_slider = Slider(ax3,      # the axes object containing the slider
                  'x',            # the name of the slider parameter
                  a_min,          # minimal value of the parameter
                  a_max,          # maximal value of the parameter
                  valinit=a_init,  # initial value of the parameter
                  #color="blue"
                 )

b_slider = Slider(ax4,      # the axes object containing the slider
                  'y',            # the name of the slider parameter
                  b_min,          # minimal value of the parameter
                  b_max,          # maximal value of the parameter
                  valinit=b_init,  # initial value of the parameter
                  #color="blue"
                 )

# Given that the picture is a square, spc.shape[0] gives its dimentions
# The dimention is scaled using the percentage from slider a (1st slider)
d=int(spc.shape[0]*10//100)
# the original picture (spc) is kept, and the scaled picture is named spd
spd = spc[d:-d,d:-d,:]
# in order to avoid modifying spd, a copy is made in spdf
spdf = spd.copy()
# counting the (unique) color values of spd, and their frequency (occurences)
color, counts = np.unique(spd[:,:,0], return_counts=True)
# Putting the color information in a single table: [[color value, count]]
cfreq = np.asarray((color, counts)).T
# in order to make graphs and calculations, separate xdata and ydata
# xdata = color values = list(color) = list(cfreq[:, 0])
# ydata = color count = list(counts) = list(cfreq[:, 1])
xdata=list(cfreq[:,0])
ydata=list(cfreq[:,1])
# fitting "func" (sum of two gaussians) on the calculated color count
# the function curve_fit is taken from scipy.optimize
# popt = optimal values for func parameters: a1, b1, c1, a2, b2, c2
# /!\ the two gaussians are not always in the same order
# pcov = estimated covariance of popt
popt, pcov = curve_fit(func, xdata, ydata, bounds=(0,[spd.shape[0]*spd.shape[1],255,255,spd.shape[0]*spd.shape[1],255,255]))
# perr = estimated standard deviation error on popt
# #perr = np.sqrt(np.diag(pcov))   # not used for now, but might be useful
# Declaring two gaussian "objects" from the results of the curve fitting
# The order hasn't been checked yet
y1=gaussian(xdata, *popt[:3])
y2=gaussian(xdata, *popt[3:])

# checks which of the two gaussians has the smallest mediane
# it is supposed here that the fibers are darker than the background
# popt[1] = a1 or a2, popt[4] = a2 or a1
# ## Note: The xoptval value for which the difference is the greatest,
# ##       might actually be zero or negative, which is a problem
# ##       for the next calculations. This problem should be solved.
if popt[1] < popt[4]:
    # Calculates the x value for which the difference between
    # the two Gaussians is the greatest. Saves this x value as xoptval
    xoptval = xdata[np.argmax(abs(y1[np.asarray(xdata)<popt[1]]-y2[np.asarray(xdata)<popt[1]]))]
else:
    # Same, but the two Gaussians are inverted
    # (maybe not needed since we take an absolute value?)
    xoptval = xdata[np.argmax(abs(y1[np.asarray(xdata)<popt[4]]-y2[np.asarray(xdata)<popt[4]]))]


if xoptval == 0:
    xoptval += 10


spdf[spd[:,:,1] < xoptval] = [255,0,0]
# current experimental part
mask=(spdf[:,:,0]==255)
while mask[mask==True].size == 0:
    xoptval += 10
    spdf[spd[:,:,1] < xoptval] = [255,0,0]
    mask=(spdf[:,:,0]==255)

spdg = (np.subtract(spd[:,1:,0],spd[:,:-1,0],dtype=int)) #abs ?
values = np.unique((abs(spdg)*(mask)[:,1:]*(mask)[:,:-1]), return_counts=True)[0]
weights = np.unique((abs(spdg)*(mask)[:,1:]*(mask)[:,:-1]), return_counts=True)[1]
weights[0]=0 ## otherwise, all the "empty" comparisons are weighted
import math
average = np.average(values, weights=weights)
variance = np.average((values-average)**2, weights=weights)
std = math.sqrt(variance)
limit = average+1.96*std #1.96
mask2 = mask.copy()
mask2[:,:-1]=(abs(spdg)>limit)
mask2[:,-1]=False
spdi=spd.copy()
spdi[mask2,0]=0
spdi[mask2,1]=255
spdi[mask2,2]=4

plt.sca(ax1)
plt.xticks(range(0,101,2))
ax1.set_xlim(-0.5, 100.5)
plt.plot(range(0,100),spdf[:100,420,1],'o', color="orange")
plt.plot(range(0,100),spdi[:100,420,2],'o', color="green")
plt.plot(range(0,100),spd[:100,420,0], 'o', color="grey")
plt.plot(range(0,100),spdg[:100,420], color="blue")
plt.axhline(y=xoptval, color='red', linestyle='-')

plt.sca(ax2)
imageobj = plt.imshow(np.column_stack(spdf[:100,420-3:420+4,:]), origin="lower")

def update(a):
    x=int(a_slider.val)
    y=int(b_slider.val)
    plt.sca(ax1)
    ax1.clear()
    plt.xticks(range(x, x+101, 2))
    ax1.set_xlim(x-0.5, x+100.5)
    plt.plot(range(x, x+100),spdf[x:x+100,y,1],'o', color="orange")
    plt.plot(range(x, x+100),spdi[x:x+100,y,2],'o', color="green")
    plt.plot(range(x, x+100),spd[x:x+100,y,0], 'o', color="grey")
    plt.plot(range(x, x+100),spdg[x:x+100,y], color="blue")
    plt.axhline(y=xoptval, color='red', linestyle='-')

    plt.sca(ax2)
    imageobj.set_data(np.column_stack(spdf[x:x+100,y-3:y+4,:]))


a_slider.on_changed(update)
b_slider.on_changed(update)

plt.show()

## Ideas for what should come next

#mask=(spdf[:,:,0]==255)
#spdg = abs(np.subtract(spd[:,1:,0],spd[:,:-1,0],dtype=int))
#values = np.unique((spdg*(mask)[:,1:]*(mask)[:,:-1]), return_counts=True)[0]
#weights = np.unique((spdg*(mask)[:,1:]*(mask)[:,:-1]), return_counts=True)[1]
#weights[0]=0 ## otherwise, all the "empty" comparisons are weighted
#import math
#average = np.average(values, weights=weights)
#variance = np.average((values-average)**2, weights=weights)
#std = math.sqrt(variance)
#limit = average+1.96*std
#mask2 = mask.copy()
#mask2[:,:-1]=(spdg>limit)
#mask2[:,-1]=False
#spdf[mask2,1]=255
##average + 2*std ## if the value seems very big, please recheck everything else
## This should give the parameters of a gaussian function representing the "jump" in color
## between two neighboring SELECTED pixels. --> fibers only, so far.
## (in one direction only... Should maybe be repeated in every direction)
## It might be possible to create the same kind of selection for the background
## and perform the same kind of analysis.
## In theory, a "jump" between a fiber and the background should be bigger than
## the average jump between two pixels of the same element. And to be 95% sure,
## normally bigger than average*1.96
## However, if the extracted average doesn't allow an easy discrimination of the
## fibers-to-background transition, the parameters could be used to draw a gaussian
## function and allow a manual or semi-automated selection.
## Maybe noise reduction is also possible.
