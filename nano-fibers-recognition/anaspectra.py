#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# // import-sort-ignore

# Todo: Comments to be added

import argparse

desc = 'Takes a filename as arguments (grayscale picture), \
        and returns the frequency of each scale of gray.'
parser = argparse.ArgumentParser(description=desc)
parser.add_argument(
    'filename',
    type=str,
    help='filename of a grayscale picture'
)
args = parser.parse_args()

print("Setting up libraries...")
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import imageio.v3 as imageio
import numpy as np
matplotlib.use('webagg')   # WebAgg #pdf? #TkAgg

print("Reading picture file...")
spc = imageio.imread(args.filename)

print("Calculating color frequencies...")
color, counts = np.unique(spc[:, :, 0], return_counts=True)
cfreq = np.asarray((color, counts)).T

print("Generating graph...")
fig = plt.figure(figsize=(15, 6))
gs1 = gridspec.GridSpec(1, 2)
ax1 = plt.subplot(gs1[0])
ax2 = plt.subplot(gs1[1])
ax1.axis('off')
ax2.axis('off')
fig.add_subplot(1, 2, 1)
plt.imshow(spc)
plt.text(
    0.5,
    -0.1,
    args.filename,
    ha='center',
    fontsize=20,
    transform=ax1.transAxes
)
fig.add_subplot(1, 2, 2)
plt.bar(cfreq[:, 0], cfreq[:, 1])
plt.text(
    0.5,
    -0.1,
    'Spectral analysis',
    ha='center',
    fontsize=20,
    transform=ax2.transAxes
)
matplotlib.pyplot.subplots_adjust(
    left=0.00,
    bottom=0.10,
    right=0.95,
    top=0.95,
    wspace=0.00,
    hspace=0.00
)
plt.show()
