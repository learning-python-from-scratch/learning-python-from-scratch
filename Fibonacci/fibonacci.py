#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import argparse


# In this program, we will try to give all variables a specific type.
# This is not a requirement in python, but might be helpful sometimes.
# an untyped (or dynamically typed) variable is declared as:
## var_name = value
# In order to give a type to this variable, the following format is used:
## var_name: var_type = value
# Functions can also have a return type, giver by "-> type"

# parse arguments
desc: str = "Returns the n first elements of the Fibonacci sequence"
# if unsure, types can be found in the interpreter with "type(<object>)"
parser: argparse.ArgumentParser = argparse.ArgumentParser(description=desc)
parser.add_argument('n', type=int, help='number of elements to print')
args: argparse.Namespace = parser.parse_args()


# Note: in python, a "class" is an object that can be reused many times.
# In other words, it is like a *type* of objects.
# For example, we could have a class named "pen" (the type of object)
## class Pen():
##      pass    # "pass" means "don't do anything"
# and then create objects of this type / class, like:
## red_pen = Pen()
## blue_pen = Pen()
# etc.

# Note 2: A class usually has an "__init__(self)" function, that does something
# when the object is created. For example:
## class Pen():
##      def __init__(self, color_string):
##          self.color = color_string
#
## red_pen = Pen("red")
## blue_pen = Pen("blue")
#
## red_pen.color == "red"  # --> TRUE
## blue_pen.color == "blue"  # --> TRUE
## red_pen.color == "blue"  # --> FALSE

# cached tuple, for storing the last 2 calculated values of the Fibonacci suite
class Cache():
    # a method (like __init__, here) has a None return type
    # self is a reference to the object itself, so there is no need to give it a type
    def __init__(self) -> None:
        self.values: List[int] = [0, 1]


# function to return the nth element based on n and a cached array (with type hints)
# note that in python3, the 'int' type can take arbitrarily large numbers (aka. bigint)
def fib_cached(n: int, cache: Cache) -> int:
    nth_value: int = 0
    if n < 2:
        nth_value = int(n)
    else:
        nth_value = cache.values[0] + cache.values[1]

    cache.values[0] = cache.values[1]
    cache.values[1] = nth_value
    return nth_value


# actually create the object based on the "Cache" class defined earlier
cache_values: Cache = Cache()
# a counter for how many elements we want to get, given as a command line argument
n: int = args.n
# main loop
i: int
for i in range(0, n):
    print(f"{int(fib_cached(i, cache_values))}")
