#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#import numpy
from PIL import Image, ImageDraw

image = Image.new("RGB", (960, 640), "#000000")
print(f"image of size {image.size} created.")
#image.show()

pix = image.load()
pix[300, 300] = (256, 256, 256)
#image.show()

draw = ImageDraw.Draw(image)
draw.line((0, 0) + image.size, fill=(128, 0, 0))
draw.polygon([(30, 30), (180, 180), (200, 160), (50, 10)], fill=(128, 256, 0))
#image.show()

image.save('image.png')
