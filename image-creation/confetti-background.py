#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy
from PIL import Image, ImageDraw

image = Image.new("RGBA", (1920, 1080), "#FFFFFF00")
print(f"image of size {image.size} created.")

number_of_dots = 1200

draw = ImageDraw.Draw(image)
#draw.line((0, 0) + image.size, fill=(128, 0, 0))
#draw.polygon([(30, 30), (180, 180), (200, 160), (50, 10)], fill=(128, 256, 0))
#image.show()

dots_alpha = 0.3
colors = [(12, 12, 69), (213, 147, 199), (203, 121, 185), (30, 20, 94), (67, 20, 94), (74, 66, 126), (85, 43, 110), (255, 219, 102)]

for i in range(0, number_of_dots):
    x_i = int(numpy.random.uniform(image.size[0]))
    y_i = int(numpy.random.uniform(image.size[1]))
    r_i = int(numpy.abs(numpy.random.normal(6, 6)))  ### make sure that r_i >= 0
    #print(x_i, y_i, r_i)
    RGBA_dots_alpha = int(numpy.random.normal(int(numpy.round(dots_alpha * 255)), 30))
    if RGBA_dots_alpha < 0 : RGBA_dots_alpha = 0
    if RGBA_dots_alpha > 255: RGBA_dots_alpha = 255
    draw.ellipse([(x_i - r_i, y_i - r_i), (x_i + r_i, y_i + r_i)], fill=(colors[numpy.random.choice(len(colors))] + (RGBA_dots_alpha,)))


image.save("background.png")
