#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ToDo: Comments to be translated
# The first two lines are the same as in hello-world.py

# Use a module that already exists, to get arguments from command line...
import argparse

# This code example was found online...
# See documentation : https://docs.python.org/3/howto/argparse.html
# From a real use case, see : power/power.py
parser = argparse.ArgumentParser(description='An example of CLI interface.')

parser.add_argument('integers', metavar='N', type=int, nargs='+',
                    help='an integer for the accumulator')
parser.add_argument('--sum', dest='accumulate', action='store_const',
                    const=sum, default=max,
                    help='sum the integers (default without: find the max)')

args = parser.parse_args()
print(args.accumulate(args.integers))
