# INTRODUCTION


Welcome to this small `python` initiation project.


## What is the learn-python-from-scratch project?

The aim of this project is to write programs using the `python` programming language, and share the code - with a lot of comments and explanations - so that others can learn and contribute as well.

It would also be interesting to suggest learning pathways. But anyone should be free to learn and contribute in their own way, and at their own pace.


## Installation

```
// ToDo : explain in details how to install python (python3) on Linux / Mac / Windows
```


## Structure

Preferentially, each program should be placed in its own **folder** together with the other files used for demonstration purposes, etc.

A good practice is to add **comments** as much as possible in the code in order to help others understand and learn.


## References

[Python official website](https://www.python.org/)

[Documentation for python 3](https://docs.python.org/3/)

[How to install and use git](http://rogerdudler.github.io/git-guide/)
(Of course, feel free to use GitLab's web interface if you want to keep things simple.)

