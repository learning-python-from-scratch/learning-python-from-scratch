#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Same first two lines as in hello-world.py
# Forces python3 to use the UTF-8 encoding for the output

# In this program, an instruction is given on screen (print("..."))
# then the user has to type something on the keyboard (function input()).
# Whatever is typed on the keyboard is saved in a variable (keyIn)
# and the content is displayed on screen for verification purposes.

print("Please type something on your keyboard.")
# "Input: " will be displayed at the beginning of the line (prompt)
keyIn = input("Input: ")
print("You wrote: " + keyIn)
